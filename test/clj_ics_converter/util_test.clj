(ns clj-ics-converter.util-test
  (:require [clojure.test :refer :all]
            [clj-ics-converter.util :refer :all]))

(def vcal-str "BEGIN:VCALENDAR|||X-WR-CALNAME:My Calendar|||BEGIN:VEVENT|||SUMMARY:An Event|||BEGIN:VALARM|||ACTION:AUDIO|||END:VALARM|||END:VEVENT|||END:VCALENDAR")
(def vevent-str "BEGIN:VEVENT|||SUMMARY:An Event|||BEGIN:VALARM|||ACTION:AUDIO|||END:VALARM|||BEGIN:VALARM|||ACTION:EMAIL|||END:VALARM|||END:VEVENT")
(def vevent-str-no-alarm "BEGIN:VEVENT|||SUMMARY:An Event|||END:VEVENT")
(def valarm-str "BEGIN:VALARM|||ACTION:AUDIO|||END:VALARM")

(deftest clean-line-folds-test
  (testing "that line folds are stripped out correctly"
    (is (= "FOO:BAR\r\nBAZ:I am a folded line"
           (clean-line-folds "FOO:BAR\r\nBAZ:I am a\r\n folded line")))))

(deftest convert-crlf-delimiters-test
  (testing "that CRLFs get converted into triple pipes"
    (is (= "foo|||bar|||baz"
           (convert-crlf-delimiters "foo\r\nbar\r\nbaz")))))

(deftest strip-repeating-delimiters-test
  (testing "that repeating delimiter groups are stripped"
    (is (= "foo|||bar|||baz|||qux" (strip-repeating-delimiters "foo|||bar||||||baz|||qux")))))

(deftest collect-event-strings-test
  (testing "that events are collected from calendar str"
    (let [event-str-seq (collect-component :VEVENT vcal-str)]
      (is (= 1 (count event-str-seq))))))

(deftest collect-alarm-strings-test
  (testing "that alarms are collected from events"
    (let [alarms (collect-component :VALARM vevent-str)]
      (is (= 2 (count alarms))))))

(deftest split-properties-test
  (testing "that ics strings can be split into their component properties"
    (let [comp-pairs (split-properties "foo:oof|||bar:rab|||baz:zab" #"\|\|\|")]
      (is (= 3 (count comp-pairs)))
      (testing "and that component pairs can be split into key value pairs"
        (is (= ["foo" "oof"] (first (map #(split-properties % #":") comp-pairs))))))))

(deftest remove-component-test
  (testing "that single vevent is removed from vcalendar"
    (let [cal "BEGIN:VCALENDAR|||BEGIN:VEVENT|||BEGIN:VALARM|||END:VALARM|||END:VEVENT|||END:VCALENDAR"]
      (is (= "BEGIN:VCALENDAR|||BEGIN:VEVENT|||END:VEVENT|||END:VCALENDAR" (remove-component :VALARM cal)))
      (is (= "BEGIN:VCALENDAR|||END:VCALENDAR" (remove-component :VEVENT cal)))))
  (testing "that multiple vevents are removed from vcalendar"
    (let [cal "BEGIN:VCALENDAR|||BEGIN:VEVENT|||END:VEVENT|||BEGIN:VEVENT|||END:VEVENT|||END:VCALENDAR"]
      (is (= "BEGIN:VCALENDAR|||END:VCALENDAR" (remove-component :VEVENT cal))))))

(deftest kv-seq-generator-test
  (testing "that gen-keyword-seq creates the correct sequence of keywords"
    (let [pairs (vector ["foo" "oof"] ["bar" "rab"] ["baz" "zab"])]
      (is (= (vector :foo :bar :baz) (ex-keyword-seq pairs)))
      (is (= (vector "oof" "rab" "zab") (ex-value-seq pairs))))))

(deftest build-component-map-test
  (testing "that a map is generated from a pair of keyword and value sequences"
    (let [ks (vector :foo :bar :baz)
          vs (vector "oof" "rab" "zab")]
      (is (= {:foo "oof" :bar "rab" :baz "zab"} (build-component-map ks vs))))))

(deftest build-vcal-map-test
  (testing "that build-vcal-map extracts the vcalendar object from an ics string"
    (let [vcal-map (build-vcal-map vcal-str)]
      (is (= "My Calendar" (:X-WR-CALNAME vcal-map)))
      (testing "and that the opening and closing tags were not included"
        (is (nil? (:BEGIN vcal-map)))
        (is (nil? (:END vcal-map))))
      (testing "as well as there being no events or alarms present"
        (is (nil? (:SUMMARY vcal-map)))
        (is (nil? (:ACTION vcal-map)))))))

(deftest build-vevent-map-test
  (testing "that build-vevent-map extracts a vevent object from a single vevent ics string"
    (let [vevent-map (build-vevent-map vevent-str)]
      (is (= "An Event" (:SUMMARY vevent-map)))
      (testing "and that the VALARM object(s) are not at the top level"
        (is (nil? (:VALARM vevent-map)))))))

(deftest build-valarm-map-test
  (testing "that build-valarm-map extracts a valarm object from a single valarm ics string"
    (let [valarm-map (build-valarm-map valarm-str)]
      (is (= "AUDIO" (:ACTION valarm-map)))
      (testing "and that the opening and closing tags were not included"
        (is (nil? (:BEGIN valarm-map)))
        (is (nil? (:END valarm-map)))))))

(deftest process-event-test
  (testing "that process-event extracts a single valarm object without the valarm keys"
    (let [vevent-map (process-event vevent-str)]
      (is (= "An Event" (:SUMMARY vevent-map)))
      (is (nil? (:BEGIN vevent-map)))
      (is (nil? (:ACTION vevent-map)))
      (testing "that the processed event has the correct number of alarms"
        (is (= 2 (count (:ALARMS vevent-map))))
        (testing "and that the alarms have the correct properties"
          (is (= "AUDIO" (:ACTION (first (:ALARMS vevent-map)))))
          (is (= "EMAIL" (:ACTION (second (:ALARMS vevent-map)))))))))
  (testing "that process-event does not create :ALARMS for sequence string without any"
    (is (nil? (:ALARMS (process-event vevent-str-no-alarm))))))

(deftest process-ics-test
  (testing "that load-ics extracts the vcalendar object from an ics string without grabbing raw vevents"
    (let [vcal-map (process-cal vcal-str)]
      (is (= "My Calendar" (:X-WR-CALNAME vcal-map)))
      (is (nil? (:SUMMARY "An Event")))
      (testing "and that events have been extracted into their own sequence of hashmaps")
      (is (= 1 (count (:EVENTS vcal-map)))))))
