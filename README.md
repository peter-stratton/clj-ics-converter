# clj-ics-converter [![Dependency Status](https://www.versioneye.com/user/projects/584ef1506949bb000e4e737b/badge.svg?style=flat-square)](https://www.versioneye.com/user/projects/584ef1506949bb000e4e737b)

A Clojure library for consuming and converting [iCalendar](https://www.ietf.org/rfc/rfc2445.txt) standards compliant data.

## Latest Version

[![Clojars Project](https://clojars.org/clj-ics-converter/latest-version.svg)](https://clojars.org/clj-ics-converter)

## The Basic Idea

iCalendar Source Data:

```
BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//hacksw/handcal//NONSGML v1.0//EN
BEGIN:VEVENT
DTSTART:19970714T170000Z
DTEND:19970715T035959Z
SUMMARY:Bastille Day Party
END:VEVENT
END:VCALENDAR
```

Becomes Clojure data structures:

```clj
{:VERSION "2.0"
 :PRODID "-//hacksw/handcal//NONSGML v1.0//EN"
 :EVENTS ({:DTSTART "19970714T170000Z" :DTEND "19970715T035959Z" :SUMMARY "Bastille Day Party"})}
```

## ToDo

- Add support for VJOURNAL, VTODO components
- cast datetime strings to object automatically

## License

Copyright © 2016

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
