(ns clj-ics-converter.load-test
  (:require [clojure.test :refer :all]
            [clj-ics-converter.load :refer :all]))

(def ics-file "test/resources/basic.ics")
(def ics-str "BEGIN:VCALENDAR\r\nX-WR-CALNAME:My\r\n Calendar\r\nBEGIN:VEVENT\r\nSUMMARY:An Event\r\nBEGIN:VALARM\r\nACTION:AUDIO\r\nEND:VALARM\r\nEND:VEVENT\r\nBEGIN:VEVENT\r\nSUMMARY:Another\r\n Event\r\nBEGIN:VALARM\r\nACTION:EMAIL\r\nEND:VALARM\r\nEND:VEVENT\r\nEND:VCALENDAR")

(deftest from-ics-string-test
  (testing "that load-ics-string loads raw ical compliant strings"
    (let [vcal (from-ics-string ics-str)]
      (is (= "My Calendar" (:X-WR-CALNAME vcal)))
      (testing "and that there are the correct number of events"
        (is (= 2 (count (:EVENTS vcal)))))
      (testing "and the events have the correct data"
        (is (= "An Event" (:SUMMARY (first (:EVENTS vcal)))))
        (is (= "AUDIO" (:ACTION (first (:ALARMS (first (:EVENTS vcal)))))))
        (is (= "Another Event" (:SUMMARY (last (:EVENTS vcal)))))
        (is (= "EMAIL" (:ACTION (first (:ALARMS (last (:EVENTS vcal)))))))))))

(deftest from-ics-file-test
  (testing "that load-ics-file loads a ical compliant file"
    (let [vcal (from-ics-file ics-file)]
      (is (= "PA Game Commission Seasons Calendar" (:X-WR-CALNAME vcal)))
      (testing "and that there are the correct number of events"
        (is (= 3 (count (:EVENTS vcal)))))
      (testing "and the events have the correct data"
        (is (= "Deer Firearms Antlered Only- See description" (:SUMMARY (first (:EVENTS vcal))))))
      (is (= "Bear Firearms WMUs 2B\\, 5B\\, 5C & 5D" (:SUMMARY (second (:EVENTS vcal)))))
      (is (= "Bear Firearms WMUs 1B\\, 2C\\, 4B\\, 4C\\, 4D & 4E" (:SUMMARY (last (:EVENTS vcal)))))
      (is (= 1 (count (:ALARMS (first (:EVENTS vcal))))))
      (is (= 1 (count (:ALARMS (second (:EVENTS vcal))))))
      (is (= 1 (count (:ALARMS (last (:EVENTS vcal))))))
      (is (= "AUDIO" (:ACTION (first (:ALARMS (first (:EVENTS vcal)))))))
      (is (= "AUDIO" (:ACTION (first (:ALARMS (second (:EVENTS vcal)))))))
      (is (= "AUDIO" (:ACTION (first (:ALARMS (last (:EVENTS vcal))))))))))
