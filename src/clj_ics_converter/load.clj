(ns clj-ics-converter.load
  "Library for loading iCalendar compliant data into native Clojure data structures."
  (:require [clj-ics-converter.util :refer :all]))

(defn from-ics-string
  "Loads a standards compliant iCalendar string into memory"
  [s]
  (let [ics-str (convert-crlf-delimiters (clean-line-folds s))]
    (process-cal ics-str)))

(defn from-ics-file
  "Loads a standards compliant iCalendar file into memory"
  [file]
  (let [ics-str (convert-crlf-delimiters (clean-line-folds (slurp file)))]
    (process-cal ics-str)))
