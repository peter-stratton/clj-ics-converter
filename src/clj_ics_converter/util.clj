(ns clj-ics-converter.util
  "Utility functions for cleaning and manipulating stringified ics data."
  (:require [clojure.string :as str]))

(def ^:private component-regex {:VCALENDAR "(BEGIN:VCALENDAR.*?END:VCALENDAR)"
                                :VEVENT "(BEGIN:VEVENT.*?END:VEVENT)"
                                :VALARM "(BEGIN:VALARM.*?END:VALARM)"})

(defn clean-line-folds
  "Strips out multiline fold designators"
  [s]
  (str/replace s #"\r\n " " "))

(defn strip-repeating-delimiters
  "Strips out sequentially repeating delimiters"
  [s]
  (str/replace s #"(\|\|\|)\1{1,}" "|||"))

(defn convert-crlf-delimiters
  "Convert CRLF delimiters to triple pipes"
  [s]
  (str/replace s #"\r\n" "|||"))

(defn collect-component
  "Collects components and their children from an ics string"
  [k s]
  (map #(first (set %)) (re-seq (re-pattern (component-regex k)) s)))

(defn remove-component
  "Removes components and their children from an ics string"
  [k s]
  (strip-repeating-delimiters (str/replace s (re-pattern (component-regex k)) "")))

(defn split-properties
  "Splits component properties into a sequence of key value pair lists."
  [s re]
  (str/split s re))

(defn ex-keyword-seq
  "Given a sequence of string pair sequences, extracts a new sequence of the first strings as keywords"
  [ss]
  (map #(keyword (first %)) ss))

(defn ex-value-seq
  "Given a sequence of string pair sequences, extracts a new sequence of the second strings"
  [ss]
  (map #(second %) ss))

(defn build-component-map
  "Given a sequence of keywords and a corresponding sequnce of values, returns a hashmap of the pairs"
  [ks vs]
  (zipmap ks vs))

(defn build-vcal-map
  "Builds a hashmap representing just the parent VCALENDAR object"
  [s]
  (let [cal-props (split-properties (remove-component :VEVENT s) #"\|\|\|")]
    (let [split-props (map #(split-properties %  #":") cal-props)]
      (let [comp-map (build-component-map (ex-keyword-seq split-props) (ex-value-seq split-props))]
        (dissoc comp-map :BEGIN :END)))))

(defn build-vevent-map
  "Builds a hashmap representing a single event component"
  [s]
  (let [event-props (split-properties (remove-component :VALARM s) #"\|\|\|")]
    (let [split-props (map #(split-properties % #":") event-props)]
      (let [comp-map (build-component-map (ex-keyword-seq split-props) (ex-value-seq split-props))]
        (dissoc comp-map :BEGIN :END)))))

(defn build-valarm-map
  "Builds a hashmap representing a single valarm component"
  [s]
  (let [alarm-props (split-properties s #"\|\|\|")]
    (let [split-props (map #(split-properties % #":") alarm-props)]
      (let [comp-map (build-component-map (ex-keyword-seq split-props) (ex-value-seq split-props))]
        (dissoc comp-map :BEGIN :END)))))

(defn process-event
  "Processes a correctly formatted ics event"
  [s]
  (let [vevent-map (build-vevent-map s)
        alarm-str-seq (collect-component :VALARM s)]
    (let [alarm-map-seq (map #(build-valarm-map %) alarm-str-seq)]
      (if (not-empty alarm-map-seq)
        (assoc vevent-map :ALARMS alarm-map-seq)
        vevent-map))))

(defn process-cal
  "Processes a correctly formatted ics calendar string"
  [s]
  (let [vcal-map (build-vcal-map s)
        event-str-seq (collect-component :VEVENT s)]
    (let [event-map-seq (map #(process-event %) event-str-seq)]
      (if (not-empty event-map-seq)
        (assoc vcal-map :EVENTS event-map-seq)
        vcal-map))))
